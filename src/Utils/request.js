import { BASE_URL } from './env';

var axios = require('axios');

var axiosInstance = axios.create({
	baseURL: BASE_URL(),
	timeout: 30000,
	/* other custom settings */
});

axiosInstance.interceptors.response.use(
	response => {
		return response;
	},
	error => {

		return Promise.reject(error.response);
	}
);

export default axiosInstance;
