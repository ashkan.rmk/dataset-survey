import requestHttp from './request';
import { BASE_URL} from './env';


export const http = requestHttp;

export const baseURL = BASE_URL();

