import * as ActionTypes from '../_constant/ActionType'; // includes Action Types constants
import * as API_URL from '../_constant/API'; // includes API URL's constants

import { http } from '../../Utils';

export const getPodcastsList = params => dispatch => {
	return new Promise((resolve, reject) => {
		http
			.get(API_URL.GET_PODCASTS)
			.then(body => {
				dispatch({
					type: ActionTypes.GET_PODCASTS_SUCCESS,
					payload: {
						podcasts: body.data,
					},
				});
				resolve(body.data);
			})
			.catch(ex => {
				dispatch({
					type: ActionTypes.GET_PODCASTS_FAILED,
					payload: {
						podcasts: [],
					},
				});
				reject({ status: 'error', msg: ex });
			});
	});
};

export const saveRating = ({ podcast, mainSubjectRating, values }) => {
	return {
		type: 'SAVE_RATING',
		payload: {
			podcast,
			mainSubjectRating,
			subRatings: values,
		},
	};
};

export const setGender = gender => {
	return {
		type: 'SET_GENDER',
		payload: {
			gender,
		},
	};
};

export const setAge = age => {
	return {
		type: 'SET_AGE',
		payload: {
			age,
		},
	};
};

export const submitToServer = () => async (dispatch, getState) => {
	let store = getState();
	let ratings = store.Podcasts.ratings;
	ratings = normalize(ratings);
	let age = store.Podcasts.age;
	let gender = store.Podcasts.gender;
	try {
		await http.post(API_URL.SUBMIT_TO_SERVER, { age, gender, ratings });
		return Promise.resolve();
	} catch (err) {
		console.log(err);
		return Promise.reject(err);
	}
};

const normalize = ratings => {
	let ratingsArray = [];
	for (let podcastId in ratings.byPodcastId) {
		let { podcast, main_subject_rate, sub_ratings } = ratings.byPodcastId[podcastId];
		let sub_ratings_array = [];
		for (let sub_rating_id in sub_ratings) {
			let sub_rating = sub_ratings[sub_rating_id];
			sub_ratings_array.push({ id: sub_rating_id, name: sub_rating.name, rate: sub_rating.rating });
		}
		ratingsArray.push({
			id: podcastId,
			podcast_name: podcast.name,
			main_subject_name: podcast.main_subject,
			main_subject_rate,
			sub_ratings: sub_ratings_array,
		});
	}
	return ratingsArray;
};
