import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import logo from '../dist/img/logo.svg';
import '../dist/css/index.css';
import 'antd/dist/antd.css';
import { Button, Icon, Divider, Radio, Select, message } from 'antd';
import * as Actions from './_actions';
import { connect } from 'react-redux';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class Start extends Component {
	state = {
		gender: 0,
		age: null,
		ageOptions: [
			'زیر 18 سال',
			'بین 18 تا 24 سال',
			'بین 25 تا 34 سال',
			'بین 35 تا 44 سال',
			'بین 45 تا 54',
			'55 به بالا',
		],
	};

	async componentDidMount() {
		await this.props.dispatch(Actions.getPodcastsList());
	}

	render() {
		return (
			<div>
				<header className="App-header">
					<img src={logo} className="App-logo" alt="logo" />

					<div style={{ direction: 'rtl', textAlign: 'center', padding: '1em 8em' }}>
						<p style={{ direction: 'rtl',lineHeight:2 }}>
							ما اینجا سعی داریم تا اطلاعات لازم برای تگ‌بندی و آموزش یک سیستم پیشنهاددهنده برای پادکست‌های فارسی
							جمع‌اوری کنیم. خوشحال میشیم تا یکم از وقتتون رو به ما بدین و این فرم رو تکمیل کنین. پیشاپیش ممنونیم ازتون
							:)
						</p>
						<p>
							برای مشاهده یک لیست کاملی از پادکست‌های فارسی هم میتونین به
							<a
								className="App-link"
								href="https://ashkanrmk.github.io/awesome-persian-podcasts/"
								target="_blank"
								rel="noopener noreferrer">
								{' اینجا '}
							</a>
							یک سری بزنین
						</p>
						<br/>
						<p>لطفا یکی رو انتخاب کنین</p>

						<RadioGroup
							defaultValue="man"
							size="large"
							style={{ direction: 'ltr' }}
							value={this.state.gender}
							onChange={e => this.setState({ gender: e.target.value })}>
							<RadioButton value={2}>
								{' '}
								<Icon type="none" />
								دیگر
							</RadioButton>
							<RadioButton value={1}>
								{' '}
								<Icon type="woman" />
								خانم
							</RadioButton>
							<RadioButton value={0}>
								{' '}
								<Icon type="man" />
								آقا
							</RadioButton>
						</RadioGroup>
						<Select
							placeholder="رده سنی خود را انتخاب کنید"
							onChange={value => this.setState({ age: value })}
							style={{ width: '200px', marginRight: 15 }}>
							{this.state.ageOptions.map((item, index) => (
								<Select.Option key={item} value={index}>
									{item}
								</Select.Option>
							))}
						</Select>
						<Divider dashed />
						<Button
							size="large"
							type="primary"
							onClick={() => {
								if (this.state.age === null) {
									message.error('لطفا رنج سنی خودتون رو انتخاب کنید');
									return;
								}

								this.props.dispatch(Actions.setAge(this.state.age));
								this.props.dispatch(Actions.setGender(this.state.gender));
								this.setState({ redirect: true });
							}}>
							شروع
							<Icon type="fire" />
						</Button>
						{this.state.redirect && <Redirect to="/start" />}
					</div>
				</header>
			</div>
		);
	}
}

const mapStateToProps = store => {
	return {};
};

export default connect(mapStateToProps)(Start);
